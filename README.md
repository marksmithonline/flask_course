# Mark smith 2018 - <https://marksmith.online>

---

## Overview

This is my first attempt at the Testdriven.io course.

---

### Getting Started:

mkdir testdriven-app && cd testdriven-app
mkdir services && cd services
mkdir users && cd users
mkdir project
python3.6 -m venv env
source env/bin/activate
(env)$ pip install flask==1.0.2