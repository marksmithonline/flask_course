# services/users/manage.py

from flask.cli import FlaskGroup  # Import FlaskGroup from flask.cli

from project import app  # import 'app' from __init__.py file in the project directory

cli = FlaskGroup(app)  # Creates a FlaskGroup instance which extends the flask.cli with commands from the 'app' application we created

if __name__ == '__main__':  # If this file is being run directly call the cli() function 
    cli() # function called if __name__ = __main__