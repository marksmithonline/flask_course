# /services/users/project/__init__.py  k

from flask import Flask, jsonify  # Import Flask and jsonify modules from flask module
import os 

app = Flask(__name__)  # set app name to Flask(__name__)

# set config
app_settings = os.getenv('APP_SETTINGS')  # new
app.config.from_object(app_settings)      # new

#import sys
#print(app.config, file=sys.stderr)

@app.route('/users/ping', methods=['GET'])  # create a route with the method GET

def ping_pong():  # define a function that will return data when the route '/users/ping' is called from the browser
    return jsonify({  # create a return a message to the browser using the jsonify module 
        'status': 'success',  # first line of message that will be returned
        'message': 'pong!'  # second line of message that will be returned
    })  # close the return statement
